// console.log('Hi');

/*
	while loop
		> takes a single condition. if the condition is true it will run the code.

		syntax:
			while(condition){
				statement
			}
*/

// let count = 5;

// while (count !== 0){
// 	console.log("while: " + count);
// 	count --;
// };

// let num = 0;

// while(num <= 5){
// 	console.log("while: " + num);
// 	num++;
// };

// Mini activity

	// let numA = 0;
	// 	while (numA <=30){
	// 	console.log('While: ' +numA);
	// 	numA += 2;
	// };


/*
	do while loop
		> this loop works a lot  like the while loop. but unlike while loops, do-while loops guarantee that the code will be executed at least once

		Syntax: 
			do {
				statement
			} while (expression/condition)
*/

// let number = Number(prompt("Type a Number"));

// do {
// 	console.log("do while: " + number);
// 	number++;
// } while (number < 10);

// number = Number(prompt("Type the second Number"));

// do {
// 	console.log("do while: " + number);
// 	number--;
// } while (number > 10);

/*
	for loop
		> more flexible than while loop and do-while loop
	parts:
		>initial value: tracks the progress of the loop.
		>condition: if true it will run the code and if false it will stop the iteration/ code
		>iteration: it indicates how to advance the loop (increasing or decreasing); final expression

		syntax:
			for(initialValue; condition; iteration){
				statemet
			}

*/

for (let count = 0; count <=5; count ++){
	console.log("for loop count: " + count);
}

let myString = "Nel"
// count the number of string
console.log(myString.length);
// shows the letter
console.log(myString[1]);

for(let x = 0; x < myString.length; x++){
	console.log(myString[x]);
};

let myName = "ginempe";

for(let n = 0; n < myName.length; n++){
	if( myName[n].toLowerCase() == 'a' ||
		myName[n].toLowerCase() == 'e' ||
		myName[n].toLowerCase() == 'i' ||
		myName[n].toLowerCase() == 'o' ||
		myName[n].toLowerCase() == 'u' ){
		console.log('Vowel')
	} else {
		console.log(myName[n]);
	};
}


let mystring = "extravagant";
let consonants = ' '
for (let x = 0; x < mystring.length; x++){
	if(
		mystring[x] == 'a' ||
		mystring[x] == 'e' ||
		mystring[x] == 'i' ||
		mystring[x] == 'o' ||
		mystring[x] == 'u'
		){

		continue;
		// console.log('Vowel ' + mystring[x])
	} else{
		// console.log(mystring[x]);
		consonants += mystring[x]
	};
};

console.log(consonants)

/* continue and break statement

	continue 
		>statement allows the code to go to the next iteration without finishing the execution of all the statements in the code block
	break
		> statement on the other hand is keyword that ends the execution of the code or the current loop

*/

for (let count = 1; count <= 20; count++){
	if(count % 5 === 0){
		console.log('Div by 5')
		continue;
	}

	console.log('coninue and break: ' + count)
	if (count > 10){
		break;
	}
}

console.log(' ');

let name = "alexander"

for(let i = 0; i < name.length; i++){
	console.log(name[i])

	if (name[i] === 'a'){
		console.log('Conitnue to the iteration')
		continue;
	}
	if(name[i] === 'd'){
		console.log('continue to the iteration')
		break;
	}
}